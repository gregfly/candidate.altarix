<?php
namespace app\base;

use app\Glob;
use app\base\Controller;

/**
 * Description of WebController
 *
 * @author Volkov Grigorii
 */
class WebController extends Controller
{
    /**
     * @param string $view
     * @param array $params
     * @return string
     */
    public function render($view, $params = [])
    {
        return Glob::$app->getView()->render($view, $params);
    }
    
    /**
     * @return string
     */
    public function getLayout()
    {
        return Glob::$app->layout;
    }
    
}
