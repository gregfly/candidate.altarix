<?php
namespace app\base;

/**
 * Description of Request
 *
 * @author Volkov Grigorii
 */
abstract class Request
{
    /**
     * @return array
     */
    abstract public function resolve();
    
}
