<?php
namespace app\base;

/**
 * Description of ConsoleRequest
 *
 * @author Volkov Grigorii
 */
class ConsoleRequest extends Request
{
    private $_params;

    /**
     * @return array
     */
    public function getParams()
    {
        if (!$this->_params) {
            if (isset($_SERVER['argv'])) {
                $this->_params = $_SERVER['argv'];
                array_shift($this->_params);
            } else {
                $this->_params = [];
            }
        }

        return $this->_params;
    }

    /**
     * @param array
     */
    public function setParams($params)
    {
        $this->_params = $params;
    }

    /**
     * @inheritdoc
     */
    public function resolve()
    {
        $rawParams = $this->getParams();
        if (isset($rawParams[0])) {
            $route = $rawParams[0];
            array_shift($rawParams);
        } else {
            $route = '';
        }

        $params = [];
        foreach ($rawParams as $param) {
            if (preg_match('/^--(\w+)(=(.*))?$/', $param, $matches)) {
                $name = $matches[1];
                $params[$name] = isset($matches[3]) ? $matches[3] : true;
            } else {
                $params[] = $param;
            }
        }

        return [$route, $params];
    }

}
