<?php
namespace app\base;

/**
 * Description of Action
 *
 * @author Volkov Grigorii
 */
class Action
{
    /**
     * @var string
     */
    public $id;
    
    /**
     * @var \app\base\Controller
     */
    public $controller;
    
    /**
     * @var string
     */
    public $runMethod = 'run';
    
    public function runWithParam($params)
    {
        if (!method_exists($this, $this->runMethod)) {
            throw new \ErrorException("Method {$this->runMethod} is not defined");
        }
        $callback = [$this, $this->runMethod];
        $missing = [];
        $args = [];
        $method = new \ReflectionMethod($callback[0], $callback[1]);
        foreach ($method->getParameters() as $param) {
            $name = $param->getName();
            if (array_key_exists($name, $params)) {
                if ($param->isArray()) {
                    $args[] = (array)$params[$name];
                } elseif (!is_array($params[$name])) {
                    $args[] = $params[$name];
                } else {
                    throw new \ErrorException("Invalid data received for parameter '{$name}'.");
                }
                unset($params[$name]);
            } elseif ($param->isDefaultValueAvailable()) {
                $args[] = $param->getDefaultValue();
            } else {
                $missing[] = $name;
            }
        }
        if (!empty($missing)) {
            throw new \ErrorException('Missing params: ' . implode(', ', $missing));
        }
        return call_user_func_array($callback, $args);
    }
    
}
