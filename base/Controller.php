<?php
namespace app\base;

/**
 * Description of Controller
 *
 * @author Volkov Grigorii
 */
class Controller
{
    /**
     * @var string
     */
    public $id;
    
    /**
     * @var string
     */
    public $defaultAction = 'default';
    
    /**
     * @param string $actionId
     * @return \app\base\Action
     */
    public function createAction($actionId)
    {
        if (is_null($actionId)) {
            $actionId = $this->defaultAction;
        }
        $inlineMethod = 'action' . ucfirst($actionId);
        if (method_exists($this, $inlineMethod)) {
            $action = new InlineAction();
            $action->controller = $this;
            $action->id = $actionId;
            $action->method = $inlineMethod;
            return $action;
        }
        $actions = $this->actions();
        if (isset($actions[$actionId])) {
            if ($actions[$actionId] instanceof Action) {
                return $actions[$actionId];
            }
            if (is_string($actions[$actionId])) {
                $className = $actions[$actionId];
                return new $className;
            }
            throw new \ErrorException('Bad action config');
        }
        throw new \ErrorException('Page not found');
    }
    
    /**
     * Actions
     * @return array
     */
    public function actions()
    {
        return [];
    }
    
}
