<?php
namespace app\base;

/**
 * Description of ErrorHandler
 *
 * @author Volkov Grigorii
 */
class ErrorHandler
{
    /**
     * @var \Exception
     */
    public $exception;
    
    public function registerHandler()
    {
        set_error_handler([$this, 'errorHandler']);
    }
    
    /**
     * Convert exception to response
     * @param \Exception $exception
     * @param \app\base\Request $request
     * @param \app\base\Response $response
     */
    public function catchException($exception, $request, $response)
    {
        $this->exception = $exception;
        $response->data = $this->convertExceptionToData($exception, $request, $response);
        $this->exception = null;
    }
    
    /**
     * Convert exception
     * @param \Exception $exception
     * @param \app\base\Request $request
     * @param \app\base\Response $response
     * @return mixed
     */
    public function convertExceptionToData($exception, $request, $response)
    {
        return get_class($exception) . "#" . $exception->getCode() . ": " . $exception->getMessage();
    }
    
    public function errorHandler($errno, $errstr, $errfile, $errline)
    {
        if (!(error_reporting() & $errno)) {
            return;
        }
        switch ($errno) {
            case E_USER_ERROR:
            echo "<b>My ERROR</b> [$errno] $errstr<br />\n";
            echo "  Фатальная ошибка в строке $errline файла $errfile";
            echo ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
            echo "Завершение работы...<br />\n";
            exit(1);
        break;
        case E_USER_WARNING:
            echo "<b>My WARNING</b> [$errno] $errstr<br />\n";
        break;
        case E_USER_NOTICE:
            echo "<b>My NOTICE</b> [$errno] $errstr<br />\n";
        break;
        default:
            echo "Неизвестная ошибка: [$errno] $errstr<br />\n";
        break;
        }
        return true;
    }
}
