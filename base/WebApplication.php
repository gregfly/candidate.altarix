<?php
namespace app\base;

/**
 * Description of WebApplication
 *
 * @author Volkov Grigorii
 */
class WebApplication extends Application
{
    /**
     * @var string
     */
    public $controllerNamespace = 'app\controllers';
    
    /**
     * @var string
     */
    public $layout = 'layouts/main';
    
    /**
     * @var string
     */
    public $defaultController = 'home';
    
    private $_response;
    
    /**
     * @return \app\base\Response
     */
    public function getResponse()
    {
        if (!$this->_response) {
            $this->_response = new WebResponse();
        }
        return $this->_response;
    }
    
    private $_view;
    
    /**
     * @return \app\base\View
     */
    public function getView()
    {
        if (!$this->_view) {
            $this->_view = new View();
        }
        return $this->_view;
    }
    
    private $_request;
    
    /**
     * @return \app\base\Request
     */
    public function getRequest()
    {
        if (!$this->_request) {
            $this->_request = new WebRequest();
        }
        return $this->_request;
    }
    
    private $_errorHandler;
    
    /**
     * @return \app\base\ErrorHandler
     */
    public function getErrorHandler()
    {
        if (!$this->_errorHandler) {
            $this->_errorHandler = new WebErrorHandler();
        }
        return $this->_errorHandler;
    }
    
}
