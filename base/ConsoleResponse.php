<?php
namespace app\base;

/**
 * Description of ConsoleResponse
 *
 * @author Volkov Grigorii
 */
class ConsoleResponse extends Response
{
    /**
     * @inheritdoc
     */
    public function getResponse()
    {
        if (is_string($this->data)) {
            return $this->data;
        }
        return print_r($this->data, true);
    }
    
}
