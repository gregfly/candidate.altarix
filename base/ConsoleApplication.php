<?php
namespace app\base;

/**
 * Description of ConsoleApplication
 *
 * @author Volkov Grigorii
 */
class ConsoleApplication extends Application
{
    /**
     * @var string
     */
    public $controllerNamespace = 'app\console\controllers';
    
    private $_response;
    
    /**
     * @return \app\base\ConsoleResponse
     */
    public function getResponse()
    {
        if (!$this->_response) {
            $this->_response = new ConsoleResponse();
        }
        return $this->_response;
    }
    
    private $_request;
    
    /**
     * @return \app\base\ConsoleRequest
     */
    public function getRequest()
    {
        if (!$this->_request) {
            $this->_request = new ConsoleRequest();
        }
        return $this->_request;
    }

}
