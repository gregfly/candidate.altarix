<?php
namespace app\base;

/**
 * Description of Application
 *
 * @author Volkov Grigorii
 */
abstract class Application
{
    /**
     * @var string
     */
    public $controllerNamespace;
    
    /**
     * @var \app\base\Action
     */
    public $action;
    
    /**
     * @var string
     */
    public $defaultController;
    
    public function run()
    {
        $this->begin();
        list($route, $params) = $this->getRequest()->resolve();
        $response = $this->runRoute($route, $params);
        $this->end($response);
    }
    
    protected function resolveRoute($route)
    {
        $parts = explode('/', ltrim($route, '/'));
        if (empty($parts) || count($parts) > 2) {
            throw new \ErrorException('Failed to resolve requested url');
        }
        return [strlen($parts[0])? $parts[0] : null, isset($parts[1]) && strlen($parts[1])? $parts[1] : null];
    }
    
    public function createController($route)
    {
        list($controllerId, $actionId) = $this->resolveRoute($route);
        if (is_null($controllerId)) {
            $controllerId = $this->defaultController;
        }
        $controllerClass = $this->controllerNamespace . '\\' . ucfirst($controllerId) . 'Controller';
        if (!class_exists($controllerClass)) {
            throw new \ErrorException("Class {$controllerClass} is not avaliable");
        }
        /* @var $controller \app\base\Controller */
        $controller = new $controllerClass();
        $controller->id = $controllerId;
        $action = $controller->createAction($actionId);
        return [$controller, $action];
    }
    
    public function begin()
    {
        $this->getErrorHandler()->registerHandler();
    }
    
    public function end($response)
    {
        $response->send();
    }
    
    /**
     * Run specific route
     * @param string $route
     * @param array $params
     */
    public function runRoute($route, $params = [])
    {
        try {
            list($controller, $action) = $this->createController($route);
            $this->getResponse()->data = $this->runAction($action, $params);
        } catch (\Exception $ex) {
            $this->getErrorHandler()->catchException($ex, $this->getRequest(), $this->getResponse());
        }
        return $this->getResponse();
    }
    
    /**
     * @param \app\base\Action $action
     */
    public function runAction($action, $params = [])
    {
        $oldAction = $this->action;
        $this->action = $action;
        ob_start();
        ob_implicit_flush(false);
        $result = $action->runWithParam($params);
        $buf = ob_get_clean();
        if (is_null($result)) {
            $result = $buf;
        }
        $this->action = $oldAction;
        return $result;
    }
    
    /**
     * @return \app\base\Response
     */
    abstract public function getResponse();
    
    /**
     * @return \app\base\Request
     */
    abstract public function getRequest();
    
    private $_errorHandler;
    
    /**
     * @return \app\base\ErrorHandler
     */
    public function getErrorHandler()
    {
        if (!$this->_errorHandler) {
            $this->_errorHandler = new ErrorHandler();
        }
        return $this->_errorHandler;
    }
    
    private $_db;
    
    /**
     * @return \PDO
     */
    public function getDb()
    {
        if (!$this->_db) {
            $this->_db = new \PDO('mysql:host=localhost;dbname=candidatealtarix', 'user', 'user');
        }
        return $this->_db;
    }
    
}
