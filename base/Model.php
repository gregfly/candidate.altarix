<?php
namespace app\base;

/**
 * Description of Model
 *
 * @author Volkov Grigorii
 */
abstract class Model
{
    /**
     * @var array
     */
    public $attributeNames = [];
    
    /**
     * Class construct
     */
    public function __construct($config = [])
    {
        $this->populateRow($config);
    }
    
    /**
     * @return array
     */
    public function getPrimaryKeys()
    {
        //get first attribute
        foreach ($this->getAttributes() as $key => $value) {
            return [$key => $value];
        }
        throw new \ErrorException('Primary keys not found');
    }
    
    /**
     * @param array|string|integer $value
     */
    public function setPrimaryKeys($value)
    {
        $value = (array)$value;
        $pks = array_keys($this->getPrimaryKeys());
        foreach ($value as $key => $val) {
            if (is_numeric($key)) {
                if (isset($pks[$key])) {
                    $key = $pks[$key];
                    $this->$key = $val;
                }
            } else if (in_array($key, $pks)) {
                $this->$key = $val;
            }
        }
    }
    
    /**
     * @return array key-value pairs
     */
    public function getAttributes($includePk = true)
    {
        $pks = [];
        if (!$includePk) {
            $pks = $this->getPrimaryKeys();
        }
        $result = [];
        foreach ($this->attributeNames as $value) {
            if (!array_key_exists($value, $pks)) {
                $result[$value] = $this->$value;
            }
        }
        return $result;
    }
    
    /**
     * @param array $attributes
     */
    public function setAttributes($attributes, $includePk = false)
    {
        $attrs = $this->getAttributes($includePk);
        foreach ($attributes as $key => $value) {
            if (array_key_exists($key, $attrs)) {
                $this->$key = $value;
            }
        }
    }

    /**
     * @param array $attributeNames
     * @return boolean
     */
    public function validate($attributeNames = [])
    {
        return !$this->hasErrors();
    }
    

    private $_errors = [];
    
    /**
     * @param string $attribute
     * @return array
     */
    public function getErrors($attribute = null)
    {
        return is_null($attribute)? $this->_errors : (isset($this->_errors[$attribute])? $this->_errors[$attribute] : []);
    }
    
    /**
     * @param string $attribute
     * @return boolean
     */
    public function hasErrors($attribute = null)
    {
        return !empty($this->getErrors($attribute));
    }
    
    /**
     * Clear current error messages
     */
    public function clearErrors()
    {
        $this->_errors = [];
    }
    
    /**
     * @param string $attribute
     * @param string $message
     */
    public function addError($attribute, $message)
    {
        if (!isset($this->_errors[$attribute])) {
            $this->_errors[$attribute] = [];
        }
        $this->_errors[$attribute][] = $message;
    }
    
    /**
     * Load data
     * @param mixed $data
     * @return boolean
     */
    public function load($data)
    {
        $this->populateRow($data);
        return true;
    }
    
    /**
     * @param array $data
     */
    public function populateRow($data)
    {
        foreach ($data as $key => $value) {
            if ($this->has($key)) {
                $this->$key = $value;
            }
        }
    }
    
    /**
     * Equals models
     * @param Model $other
     * @param boolean $strict
     * @return boolean
     */
    public function equals($other, $strict = false)
    {
        if (get_class($other) !== get_called_class()) {
            return false;
        }
        $vars = $this->getAttributes();
        foreach ($vars as $key => $value) {
            if ((!$strict || $this->$key !== $other->$key) && ($strict || $this->$key != $other->$key)) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * Equals models
     * @param Model[] $objects
     * @param Model[] $others
     * @param boolean $strict
     * @return boolean
     */
    public static function equalsObjects($objects, $others, $strict)
    {
        if (count($objects) !== count($others)) {
            return false;
        }
        foreach ($objects as $object) {
            if (!$object->equals(array_shift($others), $strict)) {
                return false;
            }
        }
        return true;
    }
    
    private $_attributes;

    public function __get($name)
    {
        if (in_array($name, $this->attributeNames)) {
            return isset($this->_attributes[$name])? $this->_attributes[$name] : null;
        }
        if (property_exists($this, $name)) {
            return $this->$name;
        }
        throw new \ErrorException("Property {$name} not exists");
    }
    
    public function __set($name, $value)
    {
        if (in_array($name, $this->attributeNames)) {
            $this->_attributes[$name] = $value;
            return;
        }
        if (property_exists($this, $name)) {
            $this->$name = $value;
            return;
        }
        throw new \ErrorException("Property {$name} not exists");
    }
    
    public function __isset($name)
    {
        return $this->has($name);
    }
    
    public  function has($name)
    {
        return !is_numeric($name) && (in_array($name, $this->attributeNames) || property_exists($this, $name));
    }
}
