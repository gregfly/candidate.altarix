<?php
namespace app\base;

/**
 * Description of Response
 *
 * @author Volkov Grigorii
 */
abstract class Response
{
    /**
     * @var mixed
     */
    public $data;
    
    /**
     * @var integer
     */
    public $exitStatus = 0;
    
    /**
     * @return string
     */
    abstract public function getResponse();
    
    /**
     * Send response
     */
    public function send()
    {
        echo $this->getResponse();
        exit($this->exitStatus);
    }
    
}
