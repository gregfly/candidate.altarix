<?php
namespace app\base;

use app\Glob;

/**
 * Description of ActiveRecord
 *
 * @author Volkov Grigorii
 */
abstract class ActiveRecord extends Model
{
    /**
     * @param boolean $runValidation
     * @param array $attributeNames
     * @return boolean
     */
    public function save($runValidation = true, $attributeNames = [])
    {
        if (empty($attributeNames)) {
            $attributeNames = array_keys($this->getAttributes(false));
        }
        if ($runValidation) {
            $this->clearErrors();
            if (!$this->validate($attributeNames)) {
                return false;
            }
        }
        return $this->internalSave($attributeNames);
    }
    
    /**
     * @return \PDO
     */
    public static function db()
    {
        return Glob::$app->getDb();
    }
    
    /**
     * @return string
     */
    public static function tableName()
    {
        $cls = new \ReflectionClass(get_called_class());
        return $cls->getShortName();
    }

    /**
     * @return boolean
     */
    public function getIsNewRecord()
    {
        foreach ($this->getPrimaryKeys() as $value) {
            if (is_null($value)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * @param array $attributeNames
     * @return boolean
     */
    protected function internalSave($attributeNames = [])
    {
        if ($this->getIsNewRecord()) {
            return $this->internalInsert($attributeNames);
        } else {
            return $this->internalUpdate($attributeNames);
        }
    }
    
    /**
     * @param array $attributeNames
     * @return boolean
     */
    protected function internalInsert($attributeNames)
    {
        $params = [];
        $q = 'INSERT INTO ' . static::tableName() . ' (' . implode(', ', $attributeNames) . ')';
        $q = $q . ' VALUES (' . implode(', ', array_fill(0, count($attributeNames), '?')) . ')';
        foreach ($attributeNames as $value) {
            $params[] = $this->$value;
        }
        $st = static::db()->prepare($q);
        if ($st->execute($params)) {
            $this->setPrimaryKeys(static::db()->lastInsertId());
            return true;
        }
        throw new \ErrorException($st->errorInfo()[2]);
    }
    
    /**
     * @param array $attributeNames
     * @return boolean
     */
    protected function internalUpdate($attributeNames)
    {
        $params = [];
        $q = 'UPDATE ' . static::tableName() . ' SET ';
        $set = [];
        foreach ($attributeNames as $value) {
             $set[] = $value . ' = ?';
             $params[] = $this->$value;
        }
        $q = $q . implode(', ', $set);
        $q = $q . ' WHERE ';
        $where = [];
        foreach ($this->getPrimaryKeys() as $key => $value) {
            $where[] = $key . ' = ?';
            $params[] = $value;
        }
        $q = $q . implode(' AND ', $where);
        $st = static::db()->prepare($q);
        if ($st->execute($params)) {
            return $st->rowCount();
        }
        throw new \ErrorException($st->errorInfo()[2]);
    }
    
    /**
     * Find all records by specific criteria
     * @param string|array $criteria
     * @return static[]
     */
    public static function findAllByCriteria($criteria = [], $params = [])
    {
        $sql = 'SELECT ' . static::tableName() . '.* FROM ' . static::tableName();
        if (empty($criteria)) {
            return self::findAllBySql($sql, $params);
        }
        $sql .= ' WHERE ';
        if (is_string($criteria)) {
            $sql .= $criteria;
        }
        if (is_array($criteria)) {
            $parts = [];
            foreach ($criteria as $key => $value) {
                if (is_string($key)) {
                    $op = '=';
                    if (is_array($value)) {
                        $op = 'IN';
                        $value = implode(', ', $value);
                    }
                    $parts[] = "{$key} {$op} {$value}";
                    continue;
                }
                if (is_array($value) && count($value) === 3) {
                    $parts[] = "{$value[1]} {$value[0]} {$value[2]}";
                    continue;
                }
                $parts[] = $value;
            }
            $sql .= implode(' AND ', $parts);
        }
        return self::findAllBySql($sql, $params);
    }
    
    /**
     * Find all records by specific $sql
     * @param string $sql
     * @return static[]
     */
    public static function findAllBySql($sql, $params = [])
    {
        $statement = static::db()->prepare($sql);
        foreach ($params as $key => $value) {
            $statement->bindValue($key, $value, \PDO::PARAM_STR);
        }
        if (!$statement->execute()) {
            throw new \ErrorException($statement->errorInfo()[2]);
        }
        
        $result = [];
        foreach ($statement->fetchAll() as $row) {
            $result[] = new static($row);
        }
        
        return $result;
    }
    
}
