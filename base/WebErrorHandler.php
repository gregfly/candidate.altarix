<?php
namespace app\base;

use app\Glob;

/**
 * Description of WebErrorHandler
 *
 * @author Volkov Grigorii
 */
class WebErrorHandler extends ErrorHandler
{
    /**
     * @var string
     */
    public $errorRoute = 'home/error';
    
    /**
     * @inheritdoc
     */
    public function convertExceptionToData($exception, $request, $response)
    {
        if ($response instanceof WebResponse) {
            if ($response->format === WebResponse::FORMAT_HTML) {
                return Glob::$app->runRoute($this->errorRoute)->data;
            }
            return [
                'name' => get_class($exception),
                'code' => $exception->getCode(),
                'message' => $exception->getMessage(),
            ];
        }
        return parent::convertExceptionToData($exception, $request, $response);
    }
    
}
