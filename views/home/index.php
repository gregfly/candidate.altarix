<?php
/* @var $this \app\base\View */
/* @var $searchModel \app\models\PingLogSearch */
/* @var $models \app\models\PingLog[] */
/* @var $rowClick \Closure */
?>
<table id="pinglog-table" border="1">
    <thead>
        <tr>
            <th>Date</th>
            <th>Ping</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($models as $index => $model): ?>
        <tr onclick="<?= isset($rowClick)? call_user_func($rowClick, $model, $index) : '' ?>">
            <td><?= $model->getBeginDate() ?></td>
            <td><?= $model->getPing() ?></td>
            <td><?= $model->getStatus() ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>