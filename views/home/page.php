<?php
/* @var $this \app\base\View */
/* @var $searchModel \app\models\PingLogSearch */
/* @var $models \app\models\PingLog[] */
/* @var $lastModel \app\models\PingLog */
/* @var $data mixed */
?>
<div class="page-part">
    <form id="pinglog-filter-form" action="" method="GET" onsubmit="return filterForm.submit(gridView.setRows.bind(gridView));">
        <div class="row" id="score-element">
            <input id="begin_date" name="begin_date" type="date" value="<?= $searchModel->getBeginDate('Y-m-d') ?>">
            <span>&nbsp;.&nbsp;.&nbsp;.&nbsp;</span>
            <input id="end_date" name="end_date" type="date" value="<?= $searchModel->getEndDate('Y-m-d') ?>">
            <span>&nbsp;</span>
            <input type="submit" value="Submit">
        </div>
    </form>
    <?= $this->renderPartial('index', [
        'searchModel' => $searchModel,
        'models' => $models,
        'rowClick' => function($model, $row) {
            return "detailView.setData(gridView.getRow({$row}))";
        },
    ]) ?>
</div>
<div class="page-part">
    <div>
        <span>Last check:</span>&nbsp;<span><?= $lastModel->getStatus() ?></span>
    </div>
    <?= $this->renderPartial('view', [
        'model' => $lastModel,
    ]) ?>
</div>
<script type="text/javascript">
    var filterForm = new SimpleForm(document.querySelector('#pinglog-filter-form'));
    var gridView = new GridView(document.querySelector('#pinglog-table'), ['begin_date', 'ping', 'status'], <?= json_encode($data) ?>);
    var detailView = new DetailView({
        'begin_date': '#detailview > #begin_date',
        'end_date': '#detailview > #end_date',
        'ping_f': '#detailview > #ping',
        'status': '#detailview > #status',
        'body': '#detailview > #body'
    });
</script>