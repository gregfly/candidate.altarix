<?php
/* @var $this \app\base\View */
/* @var $model \app\models\PingLog */
?>
<dl id="detailview">
    <dt>Start date</dt>
    <dd id="begin_date"><?= $model->getBeginDate() ?></dd>
    <dt>End date</dt>
    <dd id="end_date"><?= $model->getEndDate() ?></dd>
    <dt>Ping time</dt>
    <dd id="ping"><?= $model->getPing('{s}s ({ms}ms)') ?></dd>
    <dt>Status</dt>
    <dd id="status"><?= $model->getStatus() ?></dd>
    <dt>Body</dt>
    <dd id="body"><?= $model->getEncodedBody() ?></dd>
</dl>