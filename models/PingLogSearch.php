<?php
namespace app\models;

use app\base\Model;

/**
 * Description of PingLogSearch
 *
 * @author Volkov Grigorii
 */
class PingLogSearch extends PingLog
{
    /**
     * @inheritdoc
     */
    public function validate($attributeNames = [])
    {
        if (in_array('begin_date', $attributeNames)) {
            if (is_null($this->begin_date)) {
                $this->begin_date = date('d.m.Y');
            }
            if (is_string($this->begin_date)) {
                $this->begin_date = strtotime($this->begin_date);
            }
        }
        if (in_array('end_date', $attributeNames)) {
            if (is_null($this->end_date)) {
                $this->end_date = strtotime(date('d.m.Y') . ' +1 day');
            }
            if (is_string($this->end_date)) {
                $this->end_date = strtotime($this->end_date);
            }
        }
        return Model::validate($attributeNames);
    }
    
    /**
     * Search models
     * @param mixed $data
     * @return PingLog[]
     */
    public function search($data)
    {
        $this->load($data);
        
        if (!$this->validate($this->attributeNames)) {
            return [];
        }
        
        $criteria = [];
        if (!is_null($this->id)) {
            $criteria[] = ['=', 'id', $this->id];
        }
        if (!is_null($this->begin_date)) {
            $criteria[] = ['>=', 'begin_date', $this->begin_date];
        }
        if (!is_null($this->end_date)) {
            $criteria[] = ['<', 'end_date', $this->end_date];
        }
        
        return PingLog::findAllByCriteria($criteria);
    }
    
}
