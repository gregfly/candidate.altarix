<?php
namespace app\models;

use app\base\Model;

/**
 * Description of Taxi
 *
 * @author Volkov Grigorii
 */
class Taxi extends Model
{
    public $LicenseNum;
    public $LicenseDate;
    public $Name;
    public $OrgnNum;
    public $OrgnDate;
    public $Brand;
    public $Model;
    public $RegNum;
    public $Year;
    public $BlankNum;
    public $Info;
    
    public $attributeNames = ['LicenseNum', 'LicenseDate', 'Name', 'OrgnNum', 'OrgnDate', 'Brand', 'Model', 'RegNum', 'Year', 'BlankNum', 'Info'];
    
}
