<?php
namespace app\models;

use app\base\ActiveRecord;

/**
 * Description of PingLog
 *
 * @author Volkov Grigorii
 */
class PingLog extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $attributeNames = ['id', 'begin_date', 'end_date', 'is_success', 'body'];
    
    public $id;
    public $begin_date;
    public $end_date;
    public $is_success;
    public $body;
    
    const MAX_BODY_SIZE = 15360; // 15Kb

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ping_log';
    }
    
    /**
     * @inheritdoc
     */
    public function validate($attributeNames = array())
    {
        if (in_array('body', $attributeNames)) {
            if (strlen($this->body) > self::MAX_BODY_SIZE) {
                $this->body = substr($this->body, 0, self::MAX_BODY_SIZE);
            }
        }
        return parent::validate($attributeNames);
    }
    
    /**
     * Ping time in ms
     * @return integer
     */
    public function getPing($format = '{ms}')
    {
        $ms = ($this->end_date - $this->begin_date) * 1000;
        return strtr($format, [
            '{s}' => floor($ms / 1000),
            '{ms}' => round($ms),
        ]);
    }
    
    /**
     * Converted begin date to string
     * @param string $format
     * @return string
     */
    public function getBeginDate($format = 'h:i:s d.m.Y')
    {
        return date($format, $this->begin_date);
    }
    
    /**
     * Converted end date to string
     * @param string $format
     * @return string
     */
    public function getEndDate($format = 'h:i:s d.m.Y')
    {
        return date($format, $this->end_date);
    }
    
    /**
     * Successful name
     * @return string
     */
    public function getStatus()
    {
        return $this->is_success? 'OK' : 'FAIL';
    }
    
    /**
     * Stringify body xml to text
     * @return string
     */
    public function getEncodedBody()
    {
        return htmlspecialchars($this->body, ENT_QUOTES);
    }
    
}
