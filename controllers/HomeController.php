<?php
namespace app\controllers;

use app\Glob;
use app\base\WebController;
use app\base\WebResponse;
use app\models\PingLogSearch;
use app\models\PingLog;

/**
 * Description of HomeController
 *
 * @author Volkov Grigorii
 */
class HomeController extends WebController
{
    public function actionDefault()
    {
        $searchModel = new PingLogSearch();
        $models = $searchModel->search(Glob::$app->getRequest()->get());
        $lastModel = null;
        $data = [];
        foreach ($models as $model) {
            $data[] = [
                'id' => $model->id,
                'begin_date' => $model->getBeginDate(),
                'end_date' => $model->getEndDate(),
                'ping' => $model->getPing(),
                'ping_f' => $model->getPing('{s}s ({ms}ms)'),
                'status' => $model->getStatus(),
                'body' => $model->getEncodedBody(),
            ];
            $lastModel = $model;
        }
        if (Glob::$app->getRequest()->getIsAjax()) {
            Glob::$app->getResponse()->format = WebResponse::FORMAT_JSON;
            return $data;
        }
        return $this->render('page', [
            'searchModel' => $searchModel,
            'models' => $models,
            'lastModel' => $lastModel? : new PingLog(),
            'data' => $data,
        ]);
    }
    
    public function actionError()
    {
        return $this->render('error', [
            'exception' => Glob::$app->getErrorHandler()->exception,
        ]);
    }
    
}
