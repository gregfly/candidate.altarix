<?php
namespace app\console\controllers;

use app\Glob;
use app\base\Controller;
use app\console\actions\PingAction;
use app\models\Taxi;

/**
 * Description of CronController
 *
 * @author Volkov Grigorii
 */
class CronController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        // configure ping action
        $pingAction = new PingAction(Glob::$app->getTaxiRepository());
        $pingAction->criteria = ['RegNum' => 'ем33377'];
        $pingAction->expectedModels = [
            new Taxi([
                'LicenseNum' => '02651',
                'LicenseDate' => '08.08.2011 0:00:00',
                'Name' => 'ООО "НЖТ-ВОСТОК"',
                'OrgnNum' => '1107746402246',
                'OrgnDate' => '17.05.2010 0:00:00',
                'Brand' => 'FORD',
                'Model' => 'FOCUS',
                'RegNum' => 'ЕМ33377',
                'Year' => '2011',
                'BlankNum' => '002695',
                'Info' => '',
            ]),
        ];
        
        return [
            'ping' => $pingAction,
        ];
    }
    
}
