<?php
namespace app\console\components;

use app\base\ConsoleApplication as Application;
use app\components\SoapTaxiRepository;
use app\components\DummyTaxiRepository;

/**
 * Description of ConsoleApplication
 *
 * @author Volkov Grigorii
 */
class ConsoleApplication extends Application
{
    private $_soapClient;
    
    /**
     * @return \SoapClient
     */
    public function getSoapClient()
    {
        if (!$this->_soapClient) {
            $this->_soapClient = new \SoapClient('http://82.138.16.126:8888/TaxiPublic/Service.svc?wsdl', ['trace' => 1]);
        }
        return $this->_soapClient;
    }
    
    private $_taxiRepository;
    
    /**
     * @return \app\components\TaxiRepositoryInterface
     */
    public function getTaxiRepository()
    {
        if (!$this->_taxiRepository) {
            $this->_taxiRepository = new SoapTaxiRepository($this->getSoapClient());
//            $this->_taxiRepository = new DummyTaxiRepository();
        }
        return $this->_taxiRepository;
    }
    
}
