<?php
namespace app\console\actions;

use app\base\Action;
use app\base\Model;
use app\models\PingLog;

/**
 * Description of PingAction
 *
 * @author Volkov Grigorii
 */
class PingAction extends Action
{
    /**
     * @var \app\components\TaxiRepositoryInterface
     */
    public $taxiRepository;
    
    /**
     * @var mixed
     */
    public $criteria;
    
    /**
     * @var \app\models\Taxi[]
     */
    public $expectedModels;
    
    /**
     * @var boolean
     */
    public $strictEquals = true;
    
    /**
     * Class constructor
     */
    public function __construct($taxiRepository)
    {
        $this->taxiRepository = $taxiRepository;
    }
    
    /**
     * Run action
     * @return string
     */
    public function run()
    {
        $logModel = new PingLog();
        $logModel->begin_date = microtime(true);
        $models = $this->taxiRepository->findAll($this->criteria);
        $logModel->end_date = microtime(true);
        $logModel->is_success = Model::equalsObjects($models, $this->expectedModels, $this->strictEquals);
        $logModel->body = $logModel->is_success? '' : $this->taxiRepository->getLastRawData();
        return $logModel->save();
    }
    
}
