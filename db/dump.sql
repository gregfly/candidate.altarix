-- Adminer 4.2.2 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';

CREATE DATABASE `candidatealtarix` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `candidatealtarix`;

DROP TABLE IF EXISTS `ping_log`;
CREATE TABLE `ping_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `begin_date` double NOT NULL,
  `end_date` double NOT NULL,
  `is_success` bit(1) NOT NULL,
  `body` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- 2016-01-24 16:37:17