var ajax = {};
ajax.x = function() {
    if (typeof XMLHttpRequest !== 'undefined') {
        return new XMLHttpRequest();  
    }
    var versions = [
        "MSXML2.XmlHttp.6.0",
        "MSXML2.XmlHttp.5.0",   
        "MSXML2.XmlHttp.4.0",  
        "MSXML2.XmlHttp.3.0",   
        "MSXML2.XmlHttp.2.0",  
        "Microsoft.XmlHttp"
    ];

    var xhr;
    for(var i = 0; i < versions.length; i++) {  
        try {  
            xhr = new ActiveXObject(versions[i]);  
            break;  
        } catch (e) {
        }  
    }
    return xhr;
};

ajax.send = function(url, callback, method, data, sync) {
    var x = ajax.x();
    x.open(method, url, sync === undefined? true : sync);
    x.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    x.onreadystatechange = function() {
        if (x.readyState == 4) {
            callback(x.responseText)
        }
    };
    if (method == 'POST') {
        x.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    }
    x.send(data)
};

ajax.get = function(url, data, callback, sync) {
    if(typeof data !== 'string' && !(data instanceof String)) {
        var query = [];
        for (var key in data) {
            query.push(encodeURIComponent(key) + '=' + encodeURIComponent(data[key]));
        }
        data = query.join('&');
    }
    ajax.send(url + (data.length ? '?' + data : ''), callback, 'GET', null, sync)
};

ajax.post = function(url, data, callback, sync) {
    if(typeof data !== 'string' && !(data instanceof String)) {
        var query = [];
        for (var key in data) {
            query.push(encodeURIComponent(key) + '=' + encodeURIComponent(data[key]));
        }
        data = query.join('&');
    }
    ajax.send(url, callback, 'POST', data, sync);
};

var utils = {};

utils.toggleClass = function(o, c) {
    if (o.className.indexOf(c) >= 0) {
        this.removeClass(o, c);
    } else {
        this.addClass(o, c);
    }
};

utils.addClass = function(o, c) {
    var re = new RegExp("(^|\\s)" + c + "(\\s|$)", "g");
    if (re.test(o.className)) {
        return;
    }
    o.className = (o.className + " " + c).replace(/\s+/g, " ").replace(/(^ | $)/g, "");
};

utils.removeClass = function(o, c) {
    var re = new RegExp("(^|\\s)" + c + "(\\s|$)", "g");
    o.className = o.className.replace(re, "$1").replace(/\s+/g, " ").replace(/(^ | $)/g, "");
};

function SimpleForm(elem, action) {
    this._elem = elem;
    this._action = action || elem.getAttribute('action');
}

SimpleForm.prototype.serialize = function() {
    var form = this._elem;
    if (!form || form.nodeName !== "FORM") {
        return;
    }
    var i, j, q = [];
    for (i = form.elements.length - 1; i >= 0; i = i - 1) {
        if (form.elements[i].name === "") {
            continue;
        }
        switch (form.elements[i].nodeName) {
        case 'INPUT':
            switch (form.elements[i].type) {
            case 'text':
            case 'hidden':
            case 'password':
            case 'button':
            case 'reset':
            case 'number':
            case 'date':
            case 'submit':
                q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
                break;
            case 'checkbox':
            case 'radio':
                if (form.elements[i].checked) {
                    q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
                }
                break;
            }
            break;
            case 'file':
            break;
        case 'TEXTAREA':
            q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
            break;
        case 'SELECT':
            switch (form.elements[i].type) {
            case 'select-one':
                q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
                break;
            case 'select-multiple':
                for (j = form.elements[i].options.length - 1; j >= 0; j = j - 1) {
                    if (form.elements[i].options[j].selected) {
                        q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].options[j].value));
                    }
                }
                break;
            }
            break;
        case 'BUTTON':
            switch (form.elements[i].type) {
            case 'reset':
            case 'submit':
            case 'button':
                q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
                break;
            }
            break;
        }
    }
    return q.join("&");
};

SimpleForm.prototype.clear = function() {
    var form = this._elem;
    if (!form || form.nodeName !== "FORM") {
        return;
    }
    var i;
    for (i = form.elements.length - 1; i >= 0; i = i - 1) {
        if (form.elements[i].name === "") {
            continue;
        }
        switch (form.elements[i].nodeName) {
        case 'INPUT':
            switch (form.elements[i].type) {
            case 'text':
            case 'hidden':
            case 'password':
            case 'button':
            case 'reset':
            case 'date':
            case 'number':
            case 'submit':
                form.elements[i].value = '';
                break;
            }
            break;
        case 'TEXTAREA':
            form.elements[i].value = '';
            break;
        }
    }
    return this.errors({});
};

SimpleForm.prototype.errors = function(data) {
    var lists = document.getElementsByClassName('errors');
    for (var i = lists.length - 1; i >= 0; --i) {
        lists[i].parentNode.removeChild(lists[i]);
    }
    for (var field in data) {
        var container = document.getElementById(field + '-element');
        var errors = document.createElement('ul');
        errors.setAttribute('class', 'errors');
        container.appendChild(errors);
        for (var key in data[field]) {
            var error = document.createElement('li');
            error.textContent = data[field][key];
            errors.appendChild(error);
        }
    }
    return this;
};

SimpleForm.prototype.submit = function(callback) {
    var form = this._elem;
    var self = this;
    ajax.get(this._action, this.serialize(form), function(data) {
        callback(JSON.parse(data), self);
    });
    return false;
};

function GridView(table, columns, data) {
    this._table = table;
    this._columns = columns;
    this._data = data || [];
}

GridView.prototype.getRow = function(index) {
    return this._data[index];
};

GridView.prototype.getRows = function() {
    return this._data;
};

GridView.prototype.setRows = function(data) {
    this._data = data;
    var tbody = this._table.querySelector('tbody');
    tbody.innerHTML = '';
    var columns = this._columns;
    var index = 0;
    data.forEach(function(value) {
        var tr = document.createElement('tr');
        tr.setAttribute('onclick', 'detailView.setData(gridView.getRow(' + index + '))');
        columns.forEach(function(columnName) {
            var td = document.createElement('td');
            td.innerText = value[columnName];
            tr.appendChild(td);
        });
        tbody.appendChild(tr);
        index++;
    });
};

function DetailView(elementsMap) {
    this._elementsMap = elementsMap;
    this._data = {};
}

DetailView.prototype.getData = function() {
    return this._data;
};

DetailView.prototype.setData = function(data) {
    this._data = data;
    for (var key in this._elementsMap) {
        var value = this._elementsMap[key];
        if (typeof value === 'string' || value instanceof String) {
            value = document.querySelector(value);
        }
        value.innerHTML = data[key];
    }
};