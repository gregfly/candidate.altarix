<?php
include_once '../autoloader.php';

spl_autoload_register([new Autoloader(), 'autoload']);

\app\Glob::$app = new \app\base\WebApplication();

\app\Glob::$app->run();
