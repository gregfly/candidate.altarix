<?php
namespace app\components;

use app\models\Taxi;

/**
 * Description of DummyTaxiRepository
 *
 * @author Volkov Grigorii
 */
class DummyTaxiRepository implements TaxiRepositoryInterface
{
    /**
     * @var boolean
     */
    public $isSuccess = true;
    
    /**
     * @var integer|array
     */
    public $duration = [1, 50];
    
    /**
     * Generate random string
     * @param integer $length
     * @return string
     */
    protected function generateRandomString($length)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    
    /**
     * Sleep for current ms
     * @param integer $ms
     */
    protected function wait($ms)
    {
        $start = microtime(true);
        while (true) {
            if ((microtime(true) - $start) * 1000.0 < $ms) {
                usleep(1);
            } else {
                break;
            }
        }
    }
    
    /**
     * @inheritdoc
     */
    public function getLastRawData()
    {
        $length = rand(15000, 20000);
        return $length . " " . $this->generateRandomString($length);
    }

    /**
     * @inheritdoc
     */
    public function findAll($criteria)
    {
        $duration = $this->duration;
        if (is_array($duration)) {
            $duration = rand($duration[0], $duration[1]);
        }
        $this->wait($duration);
        if (!$this->isSuccess) {
            return [];
        }
        return [
            new Taxi([
                'LicenseNum' => '02651',
                'LicenseDate' => '08.08.2011 0:00:00',
                'Name' => 'ООО "НЖТ-ВОСТОК"',
                'OrgnNum' => '1107746402246',
                'OrgnDate' => '17.05.2010 0:00:00',
                'Brand' => 'FORD',
                'Model' => 'FOCUS',
                'RegNum' => 'ЕМ33377',
                'Year' => '2011',
                'BlankNum' => '002695',
                'Info' => '',
            ]),
        ];
    }
    
}
