<?php
namespace app\components;

/**
 * Description of TaxiRepositoryInterface
 *
 * @author Volkov Grigorii
 */
interface TaxiRepositoryInterface
{
    /**
     * @return string
     */
    public function getLastRawData();

    /**
     * Find all records by specific criteria
     * @param mixed $criteria
     * @return \app\models\Taxi[]
     */
    public function findAll($criteria);
    
}
