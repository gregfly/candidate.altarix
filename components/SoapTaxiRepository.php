<?php
namespace app\components;

use app\models\Taxi;

/**
 * Description of SoapTaxiRepository
 *
 * @author Volkov Grigorii
 */
class SoapTaxiRepository implements TaxiRepositoryInterface
{
    /**
     * @var \SoapClient
     */
    public $soapClient;
    
    /**
     * @var string
     */
    public $soapFunctionName = 'GetTaxiInfos';
    
    /**
     * @var string
     */
    public $attributePrefix = 'taxi:';
    
    /**
     * @var string
     */
    public $recordsContainer = 'GetTaxiInfosResult';
    
    /**
     * Class constructor
     */
    public function __construct(\SoapClient $soapClient)
    {
        $this->soapClient = $soapClient;
    }
    
    /**
     * @inheritdoc
     */
    public function getLastRawData()
    {
        return $this->soapClient->__getLastResponse();
    }
    
    /**
     * @inheritdoc
     */
    public function findAll($criteria)
    {
        $condition = $this->buildCondition($criteria);
        $result = $this->soapClient->__soapCall($this->soapFunctionName, $condition);
        $records = (array)$result->{$this->recordsContainer};
        $models = [];
        foreach ($records as $data) {
            $models[] = new Taxi((array)$data);
        }
        return $models;
    }
    
    /**
     * Return string
     * @param mixed $criteria
     * @return array
     */
    protected function buildCondition($criteria)
    {
        $result = '';
        foreach ($criteria as $key => $value) {
            $result .= "<{$this->attributePrefix}{$key}>{$value}</{$this->attributePrefix}{$key}>";
        }
        return [$result];
    }
    
}
